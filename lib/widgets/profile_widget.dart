import 'package:flutter/material.dart';

class Profile extends StatelessWidget {
  const Profile({
    super.key,
    required this.data,
  });

  final Map<String, dynamic> data;
  static const String _defaultImg =
      "https://images-na.ssl-images-amazon.com/images/S/pv-target-images/c139a0b1b55d64b0a3414eff0e2182626ae97550ae938b77da8debb3cbe8724f._SX1080_.png";

  @override
  Widget build(BuildContext context) {
    // return Text(data.toString());
    return _buidBody();
  }

  Padding _buidBody() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(30.0, 40.0, 30.0, 20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Center(
            child: CircleAvatar(
              backgroundImage: NetworkImage(_defaultImg),
              radius: 50.0,
            ),
          ),
          Divider(
            height: 90.0,
            color: Colors.grey[800],
          ),
          labetText(label: "NAME"),
          const SizedBox(
            height: 10,
          ),
          valueText(
            value: data["name"],
          ),
          const SizedBox(height: 30.0),
          labetText(label: "ID"),
          const SizedBox(
            height: 10,
          ),
          valueText(value: data["id"].toString()),
          const SizedBox(
            height: 30,
          ),
          labetText(label: "USERNAME"),
          const SizedBox(height: 10.0),
          valueText(value: data["username"], fontSize: 15.0),
          const SizedBox(height: 30.0),
          labetText(label: "COMPANY"),
          const SizedBox(height: 10.0),
          valueText(value: data["company"]["name"], fontSize: 15.0),
          const SizedBox(height: 30.0),
          labetText(label: "WEBSITE"),
          const SizedBox(height: 10.0),
          valueText(value: data["website"], fontSize: 15.0),
          const SizedBox(
            height: 30,
          ),
          Row(
            children: [
              Icon(Icons.email, color: Colors.grey[500]),
              const SizedBox(
                width: 8.0,
              ),
              valueText(
                  value: data["email"], fontSize: 15.0, color: Colors.grey[500])
            ],
          ),
          Row(
            children: [
              Icon(
                Icons.phone_android,
                color: Colors.grey[500],
              ),
              const SizedBox(
                width: 8.0,
              ),
              valueText(
                value: "+91${data["phone"].toString()}",
                color: Colors.grey[500],
                fontSize: 15.0,
              )
            ],
          ),
          const SizedBox(height: 30.0),
          labetText(label: "ADDRESS"),
          const SizedBox(
            height: 10.0,
          ),
          valueText(
              value:
                  "${data["address"]["street"]},${data["address"]["city"]},${data["address"]["zipcode"]}",
              fontSize: 15.0),
          const SizedBox(height: 10.0),
          valueText(
              value:
                  "LAT:${data["address"]["geo"]["lat"]},LNG:${data["address"]["geo"]["lng"]}",
              fontSize: 15.0),
          const SizedBox(
            height: 30.0,
          ),
        ],
      ),
    );
  }
}

Text valueText(
    {required String? value,
    double? fontSize = 25.0,
    Color? color = Colors.amberAccent}) {
  return Text(
    value!,
    style: TextStyle(
        color: color,
        letterSpacing: 2.0,
        fontSize: fontSize,
        fontWeight: FontWeight.bold),
  );
}

Text labetText({required String label}) {
  return Text(
    label,
    style: TextStyle(
      color: Colors.grey[500],
      letterSpacing: 2.0,
    ),
  );
}
