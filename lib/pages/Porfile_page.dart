import 'package:flutter/material.dart';

import 'package:ninja_id/widgets/profile_widget.dart';

class UserProfile extends StatelessWidget {
  const UserProfile({super.key, required this.userDatas});
  final Map<String, dynamic> userDatas;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[900],
      appBar: AppBar(
          title: const Text("Ninja"),
          backgroundColor: Colors.grey[850],
          actions: const [Icon(Icons.menu)]),
      body: SingleChildScrollView(
        child: Profile(
          data: userDatas,
        ),
      ),
    );
  }
}
