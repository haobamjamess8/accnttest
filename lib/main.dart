import 'dart:developer';
import 'package:ninja_id/pages/Porfile_page.dart';

import './widgets/profile_widget.dart';
import 'package:flutter/material.dart';

import './Model/user_model.dart';
import './fakedata/fakedatas.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: NinjaId(),
    );
  }
}

class NinjaId extends StatefulWidget {
  const NinjaId({super.key});

  @override
  State<NinjaId> createState() => _NinjaIdState();
}

class _NinjaIdState extends State<NinjaId> {
  final TextEditingController n_controller = TextEditingController();
  final TextEditingController e_controller = TextEditingController();
  final String Url_img =
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR6DmIPFKZ-VO-91fWtpbUC_Q10sgzSfcZYYw&usqp=CAU";
  List<UserModel> user = [];

  @override
  void initState() {
    user = fakedata.map((e) => UserModel.fromJson(e)).toList();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _scaffold();
  }

  Scaffold _scaffold() {
    return Scaffold(
        backgroundColor: Colors.grey[900],
        appBar: AppBar(
            leading: Icon(Icons.abc),
            title: const Text("Ninja"),
            backgroundColor: Colors.grey[850],
            actions: const [Icon(Icons.menu)]),
        body: ListView.builder(
          itemCount: user.length,
          itemBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 7.0),
              child: Card(
                child: ListTile(
                  leading: CircleAvatar(backgroundImage: NetworkImage(Url_img)),
                  trailing: IconButton(
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                            title: const Text('Option available'),
                            content: const Text("Choose option"),
                            actions: [
                              ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                      backgroundColor: Colors.red),
                                  onPressed: () {
                                    setState(() {
                                      user.removeAt(index);
                                    });
                                    Navigator.pop(context);
                                  },
                                  child: const Text(
                                    "Delete",
                                  )),
                              ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                      backgroundColor: Colors.blueAccent),
                                  onPressed: () {
                                    Navigator.pop(context);
                                    n_controller.text = user[index].name;
                                    e_controller.text = user[index].email;
                                    showDialog(
                                      context: context,
                                      builder: (context) => AlertDialog(
                                        title: Text(
                                            "Edit info for ID:${user[index].id}"),
                                        content: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              SizedBox(
                                                width: 250,
                                                child: TextField(
                                                    keyboardType:
                                                        TextInputType.name,
                                                    controller: n_controller,
                                                    decoration:
                                                        const InputDecoration(
                                                            counterText: "",
                                                            hintText: "Name")),
                                              ),
                                              SizedBox(
                                                width: 250,
                                                child: TextField(
                                                  keyboardType: TextInputType
                                                      .emailAddress,
                                                  controller: e_controller,
                                                  decoration:
                                                      const InputDecoration(
                                                          counterText: "",
                                                          hintText: "email"),
                                                ),
                                              )
                                            ]),
                                        actions: [
                                          ElevatedButton(
                                            onPressed: () {
                                              final String name =
                                                  n_controller.text;
                                              final email = e_controller.text;
                                              setState(() {
                                                user[index].name = name;
                                                user[index].email = email;
                                              });
                                              Navigator.pop(context);
                                            },
                                            style: ElevatedButton.styleFrom(
                                                backgroundColor: Colors.green),
                                            child: const Text("Ok"),
                                          )
                                        ],
                                      ),
                                    );
                                  },
                                  child: const Text(
                                    "Edit",
                                  )),
                            ],
                          ),
                        );
                      },
                      icon: Icon(
                        Icons.more_vert_rounded,
                        color: Colors.grey[500],
                      )),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => UserProfile(
                                  userDatas: user[index].toJson(),
                                )));
                  },
                  tileColor: Colors.grey[800],
                  title: Text(
                    user[index].name,
                    style: TextStyle(color: Colors.grey[300]),
                  ),
                  subtitle: Text(
                    user[index].email,
                    style: TextStyle(color: Colors.grey[500]),
                  ),
                ),
              ),
            );
          },
        ));
  }
}
